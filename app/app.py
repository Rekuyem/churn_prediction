import dash
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html
import pandas as pd
import numpy as np
from dash.dependencies import Output, Input
import seaborn as sns
import warnings
warnings.simplefilter(action="ignore", category=FutureWarning)
import matplotlib.pyplot as plt
import plotly.express as px
import plotly.graph_objs as go
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split


from imblearn import over_sampling,under_sampling
from imblearn.over_sampling import SMOTE

from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_auc_score
from sklearn import metrics
from matplotlib import pyplot
import math
from sklearn.metrics import roc_curve
from sklearn.metrics import accuracy_score
import warnings
import math
from sklearn.model_selection import learning_curve
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split,cross_validate
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.filterwarnings("ignore")

#tab style 
tabs_styles = {
    'height': '44px',
}
tab_style = {
    'borderBottom': '1px solid #d6d6d6',
    'padding': '6px'
    #'fontWeight': 'bold'
}
tab_selected_style = {
    'borderTop': '1px solid #d6d6d6',
    'borderBottom': '1px solid #d6d6d6',
    'backgroundColor': 'rgb(149, 133, 255)',
    'color': 'white',
    'padding': '6px'
}
FONT_AWESOME = "https://use.fontawesome.com/releases/v5.7.2/css/all.css"
app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP,FONT_AWESOME])
server = app.server
#app.css.append_css({'external_url': 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'})
app.layout = html.Div([
    dcc.Tabs(id="tabs-styled-with-inline", value='tab-1', children=[
        dcc.Tab(label='Dashboard', value='tab-1', style=tab_style, selected_style=tab_selected_style),
        dcc.Tab(label='Marketing', value='tab-2', style=tab_style, selected_style=tab_selected_style),
    ], style=tabs_styles),
    html.Div(id='tabs-content-inline')
])


#### Definition de parametres

df = pd.read_csv('WA_Fn-UseC_-Telco-Customer-Churn.csv')
index = df.columns.values
moyenne = np.mean(df['MonthlyCharges'])
tab=df.describe()
tab=round((len(df.loc[df['Churn'] == 'Yes'])/len(df['Churn']))*100,2)



############## Modèle de prédiction#################

def feature_important():
    cleaned_df = df.drop('customerID', axis=1)
    for column in cleaned_df.columns:
            if cleaned_df[column].dtype == np.number:
                  continue
            cleaned_df[column] = LabelEncoder().fit_transform(cleaned_df[column])
    
    #Mettre à l'échelle les données nettoyées
    X = cleaned_df.drop('Churn', axis = 1) 
    feature_names= X.columns.values
    y = cleaned_df['Churn']
    #Standardisation / mise à l'échelle des valeurs
    X = StandardScaler().fit_transform(X)
    grid_S = {'C':np.logspace(-3,3,7), 'penalty':['l1','l2']} 
    #Créer le modéle
    model = LogisticRegression()
    #Optimiser paramétres du modéle
    model_cv = GridSearchCV(model, grid_S, cv=10 , scoring='roc_auc')

    skf = StratifiedKFold(n_splits=5)
    lst_accu_stratified = []
    k_fold_predictions = np.array([])
    k_fold_labels = np.array([])
    fold_nmbr=1
    for train_index, test_index in skf.split(X, y): 
    
        X_train_fold, X_test_fold = X[train_index], X[test_index] 
        y_train_fold, y_test_fold = y[train_index], y[test_index] 
        #print("Fold : ",fold_nmbr)
        fold_nmbr=fold_nmbr+1
        model_cv.fit(X_train_fold, y_train_fold) 
        lst_accu_stratified.append(model_cv.score(X_test_fold, y_test_fold))
        # predict probabilities
        lr_probs = model_cv.predict_proba(X_test_fold)
        k_fold_predictions = np.append(k_fold_predictions,lr_probs[:, 1])
        k_fold_labels = np.append(k_fold_labels, y_test_fold)
        #print("Accuracy:",metrics.accuracy_score(y_test_fold, np.round(lr_probs[:, 1])))  
    feature_importance = pd.DataFrame(feature_names, columns = ["feature"])
    feature_importance["importance"] = pow(math.e, model_cv.best_estimator_.coef_[0])
    feature_importance = feature_importance.sort_values(by = ["importance"], ascending=False)
    K = feature_importance.plot.barh(x='feature', y='importance')
    #aff=print(feature_importance)
   
    return feature_importance
    

feature_important =feature_important()
feature_importance=feature_important[0:3]
dict_feature_correction = {'MonthlyCharges':'Quand on compare les charges mensuelles avec la fidélité du client, les chiffres sont formels. Les clients les plus fidèles ont des charges comprises entre 20$ et 30$. Les clients qui paient plus cher ont plus tendance à se désabonner. Plus le client paie cher son abonnement, plus il aura tendance à vouloir se désabonner. Il faudra penser à intégrer dans notre programme de fidélité le prix idéal qui va faire la balance entre la rentabilité et le désabonnement. ',
                           'InternetService':'Le type de service internet. D’après les résultats de l’audit, les clients en DSL sont plus fidèles au contraire des clients qui possèdent la fibre optique. Le nombre d’utilisateurs sans connexion internet est négligeable. Cette analyse est très intéressante mais inquiétante car de plus en plus de clients basculent vers la fibre optique. Par conséquent, si nous n’agissons pas maintenant, le désabonnement va s’accroître car nos offres sur la fibre optique ne sont pas attractives pour les nouveaux clients.',
                           'PaperlessBilling' :'Il y a aussi un paramètre important à prendre en compte : c’est la facturation sans papier. En effet, les clients avec une facturation sans papier sont les plus suspectibles a se désabonner. Il faudra donc obliger les clients à régler la facture du nouveau forfait via une facturation papier et non en format numérique.',
                           'TotalCharges':'',
                           'PaymentMethod':'',
                           'SeniorCitizen':'',
                           'MultipleLines' : '',
                           'Partner':'',
                           'StreamingTV' : '',
                           'StreamingMovies': '',
                           'gender' : '',
                           'DeviceProtection' : '',
                           'Dependents':'',
                           'OnlineBackup':'' ,
                           'OnlineSecurity' :'',
                            'TechSupport':'',
                           'PhoneService': '',
                           'Contract':'',
                           'tenure' : ' ',
                           }

feature_recommandation = feature_importance[0:3]
recommandation=[]
for index,row in feature_recommandation.iterrows():
  recommandation.append(dict_feature_correction.get(row['feature']))
feature_recommandation['correction']=recommandation
feature_recommandation= feature_recommandation[['feature','correction']]
recommandation = feature_recommandation.to_numpy()
#1

chargesMensuelles = px.histogram(df, x='MonthlyCharges', color='Churn',
                labels={
                     "MonthlyCharges": "Charges mensuelles",
                     "Churn": "Churn"
                 },
                title="Charges mensuelles et churn")

chargesTotal = px.histogram(df, x='TotalCharges', color='Churn',
                labels={
                     "TotalCharges": "Charges totales",
                     "Churn": "Churn"
                 },
                title="Charges totales et churn")                


# Longévité du client

tenure = px.histogram(df, x='tenure', color="Churn", barmode='group',
                labels={
                     "tenure": "Tenance",
                     "Churn": "Churn"
                 },
                title="Nombre de mois souscrit à l'entreprise")

# Contrat

dependent = px.histogram(df, x='PaymentMethod', color="Churn", barmode='group',
                labels={
                     "PaymentMethod": "Méthodes de paiement",
                     "Churn": "Churn"
                 },
                title="Churn par rapport aux méthodes de paiement")

typeContrat = px.histogram(df, x='Contract', color="Churn", barmode='group',
                labels={
                     "Contract": "Contrat",
                     "Churn": "Churn"
                 },
                title="Churn par rapport aux contrats")

typeContrat2 = px.histogram(df, x='Contract', 
                labels={
                     "Contract": "Contrat"
                 },
                title="Somme des types de contrat")

# Service Divers

typeInternet = px.histogram(df, x='InternetService', color="Churn", barmode='group',
                labels={
                     "InternetService": "Types d'internet",
                     "Churn": "Churn"
                 },
                title="Type d'internet et churn associé")

cols = ['OnlineSecurity','OnlineBackup','DeviceProtection','TechSupport','StreamingTV','StreamingMovies']
df1 = df[(df['Churn'] == 'Yes') & (df['InternetService'] != 'No')][cols]
df2 = df[df['InternetService'] != 'No'][cols]
df1 = pd.melt(df1).rename({'value':'Has Service'},axis=1)
df2 = pd.melt(df2).rename({'value':'Has Service'},axis=1)

typeInternetChurn = px.histogram(df1, x='variable', color="Has Service", barmode='group',
                labels={
                     "variable": "Services internet additionnels",
                     "count" : "Churns"
                 })

typeInternetChurn2 = px.histogram(df2, x='variable', color="Has Service", barmode='group',
                labels={
                     "variable": "Services internet additionnels",
                 })



#### Style

TITLE_PARAPH={
    'backgroundColor': 'rgb(115 128 253)',
    'color': 'white',
    'padding': '5px'
}

STYLE_PARAGH={
    'text-align': 'justify'
    
}


TEXT_STYLECard = {
    'textAlign': 'center',
    'color': '#FFFFFF'
}

SIDEBAR_STYLE = {
    'position': 'fixed',
    'top': 0,
    'left': 0,
    'bottom': 0,
    'width': '10%',
    'padding': '20px 20px',
    'background-color': '#f8f9fa'
}

CARD_TEXT_STYLE = {
    'textAlign': 'center',
    'color': '#0074D9'
}

CARD_STYLE = {
    'padding': '50px',

}

TEXT_STYLE = {
    'textAlign': 'center',
    'color': '#191970',
    'padding':'10px'

}

CONTENT_STYLE = {
    'margin-left': '5%',
    'margin-right': '5%',
    'padding': '20px 20px',
    'background-color': '#f8f9fa'
}


TITLE = {
    'background-color': '#9585ff',
    'color': 'white',
    'padding': '5px'
}

TITLE_TWO = {
    'background-color': 'rgb(115 128 253)',
    'color': 'white',
    'padding': '5px'
}

TITLE_THREE = {
    'background-color': 'rgb(68 85 251)',
    'color': 'white',
    'padding': '5px'
}

TITLE_FOUR = {
    'background-color': '#2E2EFE',
    'color': 'white',
    'padding': '5px'
}
TITLE_FIVE= {
    'background-color': 'rgb(21 37 187)',
    'color': 'white',
    'padding': '5px'
}

# Contenu 

cartes = dbc.Row(
    [
        dbc.Col(dbc.Card(
            [

                dbc.CardBody(
                    [
                        html.H4(id='card_title_1', children=['Totale clients'], className='card-title',
                                style=TEXT_STYLECard),
                        html.P(id='card_text_1', children=[len(df)], style=TEXT_STYLECard),
                    ]
                )
            ], color="primary"
        )),

        dbc.Col(dbc.Card(
            [
                dbc.CardBody(
                    [
                        html.H4('Moyenne charges', className='card-title', style=TEXT_STYLECard),
                        html.P('64.76 $', style=TEXT_STYLECard),
                        
                    ]
                ),
            ],color="secondary"

        )),

        dbc.Col(dbc.Card(
            [
                dbc.CardBody(
                    [
                        html.H4('Churn client', className='card-title', style=TEXT_STYLECard),
                        html.P(np.sum(df['Churn'] == 'Yes'), style=TEXT_STYLECard),
                    ]
                ),
            ],color="info"

        )),

        dbc.Col(dbc.Card(
            [
                dbc.CardBody(
                    [
                        html.H4('% Churn', className='card-title', style=TEXT_STYLECard),
                        html.P(str(tab) + ' %', style=TEXT_STYLECard),
                    ]
                ),
            ],color="danger"
        ))                  
    ], style=CARD_STYLE
)

content_first_row = dbc.Row(
        
    [      

        dbc.Col(
            dbc.FormGroup(
                [
                    html.H6('Répartition globale'),
                    dcc.Dropdown(
                        id='my_dropdown',
                        options=[
                                {'label': 'Genre', 'value': 'gender'},
                                {'label': 'Méthodes de paiement', 'value': 'PaymentMethod'},
                                {'label': 'Types de contrats', 'value': 'Contract'}
                        ],
                        value='gender',                                                                  
                        multi=False,
                        clearable=False
                    ), 
                    dcc.Graph(id='pie-chart')
                ]
            ), md=6),

        dbc.Col(
            dbc.FormGroup(
                [
                    html.H6('Profiling des clients'),
                    dcc.Dropdown(
                        id='dropdown-histo',
                        options=[
                                {'label': 'Situation', 'value': 'Partner'},
                                {'label': 'Dépendance', 'value': 'Dependents'},
                                {'label': 'Genre', 'value': 'gender'},
                                {'label': 'Retraités', 'value': 'SeniorCitizen'}
                        ],
                        value='Dependents',                                                                  
                        multi=False,
                        clearable=False
                    ), 
                    dcc.Graph(id='histo')
                ]
            ), md=6),            
        
    ]
)


# Titre 1

content_title_row = dbc.Row(
        
    [      
        dbc.Col(
            html.H6("Les types d’abonnement de service et ses charges"),md=12
        )
    ],
    style=TITLE
)
 

# Contenu 1

content_second_row = dbc.Row(
        
    [   
        dbc.Col(
            dcc.Graph(figure=typeInternet), md=4  
        ),   
        dbc.Col(
            dcc.Graph(figure=chargesMensuelles), md=4 
        ),
        dbc.Col(
            dcc.Graph(figure=chargesTotal), md=4
        )
    ]
)

# Titre 2

content_title2_row = dbc.Row(
        
    [      
        dbc.Col(
            html.H6('Les Types d’offre et son moyen de paiement'),md=12
        )
    ],
    style=TITLE_TWO
)


# Contenu 2

content_third_row = dbc.Row(

    [
        dbc.Col(
            dcc.Graph(figure=dependent), md=4, 
        ),
        dbc.Col(
            dcc.Graph(figure=typeContrat), md=4, 
        ),
        dbc.Col(
            dcc.Graph(figure=typeContrat2), md=4, 
        )                
    ]

)

# Titre 3

content_title3_row = dbc.Row(
        
    [      
        dbc.Col(
            html.H6("Les services utilisateurs"),md=12
        )
    ],
    style=TITLE_THREE
)

# Contenu 3

content_fourth_row = dbc.Row(
    [
        dbc.Col(
            dcc.Graph(figure=typeInternetChurn2), md=6  
        ),
        dbc.Col(        
                dcc.Graph(figure=typeInternetChurn),md=6 # deuxieme
            ),            
    ]
)

# Titre 4

content_title4_row = dbc.Row(
        
    [      
        dbc.Col(
            html.H6('Temporalité'),md=12
        )
    ],
    style=TITLE_FOUR
)

# Contenu 4

content_fifth_row = dbc.Row(
    [
        dbc.Col(
            dcc.Graph(figure=tenure), md=12 # 24
        )
    ],
)
# Contenu 5

content_title5_row = dbc.Row(
        
    [      
        dbc.Col(
            html.H6('Les variables par pertinences (à partir du modèle)'),md=12
        )
    ],
    style=TITLE_FIVE
)
# Contenu 6

content_six_row = dbc.Row(
    [
        dbc.Col(
             dcc.Graph(id='graph_4',
                        figure= {
                            "data": [
                                {
                                    "y": feature_important['importance'],
                                    "x": feature_important['feature'],
                                    "type": "bar",
                                    'marker' : { "color" : 'rgb(115 128 253)'}
                                                            
                                    
                                    
                                },
                            ],
                            "layout": {
                                'xaxis': {
                                    'title': 'Ordoner par importance',
                                },
                                "title": {"x": 0.05, "xanchor": "left"},
                            
                               

                              

                                

                            },
                        }
                    ), md=12,
        )
    ]
)

#######################Marketing#############################
content_mark_part1 = dbc.Row(
        
    [      
        dbc.Col(
                   [ html.H6("Recommandation 1 :",style=TITLE_PARAPH),
                    html.P(recommandation[0,1],style=STYLE_PARAGH),
        
                   ],md=6
        
        ),
        dbc.Col(
                   [ html.H6("Recommandation 2 :",style=TITLE_PARAPH),
                    html.P(recommandation[1,1],style=STYLE_PARAGH),
              
                   ],md=6
        
        ),
        dbc.Col(
                   [ html.H6("Recommandation 3 :",style=TITLE_PARAPH),
                    html.P(recommandation[2,1],style=STYLE_PARAGH),
              
                   ],md=6
        
        )
    ], style=CARD_STYLE
)




content = html.Div(
    [
        html.H2('Dashboard Churn', style=TEXT_STYLE),
        cartes,
        content_first_row,
        content_title_row,
        content_second_row,
        content_title2_row, 
        content_third_row,
        content_title3_row,
        content_fourth_row,
        content_title4_row,
        content_fifth_row,
        content_title5_row,
        content_six_row        
    ],
    style=CONTENT_STYLE
)
content2 = html.Div(
    [
        html.Hr(),
        html.H2('Analyse Marketing', style=TEXT_STYLE),
        html.Hr(),
        content_mark_part1
    ],
)


@app.callback(Output('tabs-content-inline', 'children'),
              [Input('tabs-styled-with-inline', 'value')])
def render_content(tab):
    if tab == 'tab-1':
        return    html.Div([content])
 
        
    elif tab == 'tab-2':
        return html.Div([content2])


@app.callback(
    Output(component_id='pie-chart', component_property='figure'),
    [Input(component_id='my_dropdown', component_property='value')]
)

def update_graph(my_dropdown):
    dff = df

    piechart=px.pie(
            data_frame=dff,
            names=my_dropdown,
            hole=.3,
            )

    return (piechart)

@app.callback(
Output("histo", "figure"), 
[Input("dropdown-histo", "value")])
def update_bar_chart(colonne):
    fig = px.histogram(df, x=colonne, color="Churn", barmode='group')
    return fig
   

if __name__ == "__main__":
    app.run_server()
